using MiniFB;
import VideoIO;

function mandel(frameSize = 1000, WIDTH = 1920, HEIGHT = 1016, iterations = 100, offsetx = .75, offsety = 0)
	stepSize = 3.25 / frameSize;
	#loadLength = 80;
	#loadAnimation = ['_', '-', '¯', '-'];
	#window = mfb_open_ex("Mandelbrot", WIDTH, HEIGHT, MiniFB.WF_RESIZABLE);
	#mfb_set_target_fps(10);
	buff = zeros(UInt32, WIDTH * HEIGHT);
	print("\r");
	for x = 1:WIDTH
		for y = 1:HEIGHT
			lx = (x - WIDTH / 2) * stepSize - offsetx;
			ly = (y - HEIGHT / 2) * stepSize - offsety;
			n = complex(lx, ly);
			N = n
			for i = 1:iterations
				N = (N)^2 + n;
				if abs(N) > 255 | isnan(N)
					break;
				end
			end
			intens = abs(n);
			if isreal(abs(N));
				N = floor(abs(N));
				intens = minimum([N / 255, 255]);
				intens = ceil(intens);
			end
			#percent = ((x-1) * HEIGHT + y) / (WIDTH * HEIGHT);
			#print("[", repeat("|", floor(Int, (loadLength - 2) * percent)), loadAnimation[floor(Int, (loadLength - 2) * percent * 10 % 4 + 1)], repeat(".", floor(Int, loadLength - 2 - ((loadLength - 2) * percent))), "]", "\r");
			red = round(UInt8, minimum([255, !isnan(intens / abs(n)) ? intens/abs(n) : 0]));
			green = intens;
			blue = round(UInt8, minimum([255, !isnan(intens / abs(n)) ? intens/abs(n) : 0]));
			buff[(y-1)*WIDTH + x] = mfb_rgb(red, green, blue);
			#if y % HEIGHT == 0
			#	mfb_update(window, buff);
			#end
		end
	end
	#while mfb_wait_sync(window)
	#	state = mfb_update(window, buff)
	#end
	return buff
end

__precompile__();

#function mandel(frameSize = 1000, WIDTH = 1920, HEIGHT = 1016, iterations = 100, offsetx = .75, offsety = 0)

begin;
	println("Starting...");
	#window size
	WIDTH = 960;
	HEIGHT = 508;
	#zoom path variables
	zoomStart = 1000;
	zoomEnd = 10000;
	centerStartX = .75;
	centerEndX = 1.2;
	centerStartY = 0;
	centerEndY = 0.3;
	startIterations = 150;
	endIterations = 151;
	#steps to take between the start and stop
	steps = 10;

	#make the iteratiors for the frames
	zoomPath = zoomStart:(zoomEnd-zoomStart)/steps:zoomEnd;
	centerXPath = centerStartX:(centerEndX - centerStartX)/steps:centerEndX;
	centerYPath = centerStartY:(centerEndY - centerStartY)/steps:centerEndY;
	iterationsPath = floor.(UInt, startIterations:(endIterations - startIterations)/steps:endIterations);

	#make the window
	window = mfb_open_ex("Mandelbrot", WIDTH, HEIGHT, MiniFB.WF_RESIZABLE);
	mfb_set_target_fps(10);

	#make the video saving shiz
	encoder_options = (crf=0, preset="ultraslow");

	#preallocate arrays
	buff = zeros(UInt32, WIDTH*HEIGHT); #mandel(zoomStart, WIDTH, HEIGHT, startIterations, centerStartX, centerStartY)
	imgs = zeros(UInt32, WIDTH*HEIGHT*steps);

	println(typeof(buff), ":", sizeof(buff), "-", typeof(imgs[((1 - 1) * sizeof(buff) + 1):((sizeof(buff)) * 1)]), ":", ((1 - 1) * sizeof(buff) + 1), ":", (sizeof(buff) * 1), ":(", (sizeof(buff) * 1) - ((1 - 1) * sizeof(buff) + 1));

	for i = 1:steps
		println("Step ", i, "...");
		buff = mandel(zoomPath[i], WIDTH, HEIGHT, iterationsPath[i], centerXPath[i], centerYPath[i]);
		for i = 1:100;
			state = mfb_update(window, buff);
		end

		imgs[(i - 1) * round(UInt64, sizeof(buff) / 4) + 1:(round(UInt64, sizeof(buff) / 4) * i)] = buff[:];

		println(zoomPath[i], ' ', WIDTH, ' ', HEIGHT, ' ', iterationsPath[i], ' ', centerXPath[i], ' ', centerYPath[i])
	end

	#reshape the image stack into the final format


	#output the video
	#VideoIO.save("output.mp4", imgs, framerate=2, encoder_options=encoder_options);

	println("Done!");
	#while mfb_wait_sync(window)
	#	state = mfb_update(window, buff);
	#end
end;

__precompile__();
